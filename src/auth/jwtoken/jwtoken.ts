/* 
    Clase que generar un token valido, valida si el token es enviado y si es asi valida si el token es valido
    Para más implementación la página oficial de la libreria https://www.npmjs.com/package/jsonwebtoken 
*/

import { Router,  Request, Response, NextFunction  } from 'express';
import jsonwebtoken from 'jsonwebtoken';
import config from './config'

class JWebToken {
    private router: Router = Router();
    constructor() {}

    public token(): String { return jsonwebtoken.sign({ cheked: true }, config.key, { expiresIn: config.expiresIn }); }

    public routeProtected: Router = this.router.use( ( req: Request, res: Response, next: NextFunction) => {
        // Recibiendo el token desde el header
        //const token  = req.headers['authorization']?.replace('Bearer ', '')
        // Recibiendo el token desde el body
        const { token }  = req.body
        token ? jsonwebtoken.verify(token, config.key, (err: any, decoded: any) => err ? res.json({ status: false, message: 'Token no valido' }) : next() )
              : res.json({ status: false, message: 'Sin autorización' })
    })

 }

 export const webToken = new JWebToken();