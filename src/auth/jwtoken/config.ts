/* 
    Objecto de configuracion para libreria JwToken, 
    Key: es la llave de cifrado con la que se generar el token,
    expiresIn: Es el tiempo de duración del token donde 1440 / 60(segundos) = 24 minutos
               Por lo tanto para prolongar el tiempo se debe multiplicar minutos * 60(segundos) 
*/

export default {
    key: 'secret-2020',
    expiresIn: 1440
}