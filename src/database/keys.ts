/* 
    Objecto de conexión para la base de datos de mysql
    host: Localhost(si la base de datos es local) o ip del servidor( donde se encuentra alojado la base de datos).
    user: Por defecto es root.
    password: contraseña para la conexión al servidor de base de datos
    database: nombre de la base de datos a utilizar
 */

export default {
    database: {
        host: '',
        user: '',
        password: '',
        database: ''
    }
}