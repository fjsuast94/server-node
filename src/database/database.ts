/* 
    Libreria que permite conectarnos a la base de datos de mysql, 
    se necesita un objecto de configuración para establecer la conexión
*/

import mysql from 'promise-mysql';
import keys from './keys';

const pool = mysql.createPool(keys.database);

pool.getConnection()
    .then( connection => {
        pool.releaseConnection(connection);
        console.log('DB is Connected');
    })
    .catch( error => {
        console.error( error );
    });

export default pool;