import { Request, Response } from 'express';
import pool from '../database/database';

import { webToken  } from "../auth/jwtoken/jwtoken";


class IndexController {

    public async login( req:Request, res:Response ): Promise<any> {
        
        const { correo, password } = req.body;

        if( !correo ) {
            return res.json({
                status: false,
                message: 'El campo correo electrónico es requerido' 
            });
        }

        if( !password ) {
            return res.json({
                status: false,
                message: 'El campo contraseña es requerido' 
            });
        }

        const login = await pool.query('CALL Login(?, ?)', [correo, password]);
        const response = login[0][0];

        if( !response.status ) {
            return res.json({
                status: response.status,
                message: response.message
            });

        } else {
            return res.json({
                status: response.status,
                data: response,
                token: webToken.token()
            });
        }
    }
    
    public async registro( req: Request, res: Response ): Promise<any> {
        const { nombre, apellidoPaterno, apellidoMaterno, password, role, email, google } = req.body;

        if( !nombre ) {
            return res.json({
                status: false,
                message: 'El nombre es requerido.' 
            });
        }

        if( !apellidoPaterno ) {
            return res.json({
                status: false,
                message: 'El apellido paterno es requerido.' 
            });
        }

        if( !password ) {
            return res.json({
                status: false,
                message: 'La contraseña es requerida.' 
            });
        }

        if( !email ) {
            return res.json({
                status: false,
                message: 'La correo es requerido.' 
            });
        }

        const params = [
            nombre,
            apellidoPaterno,
            apellidoMaterno,
            email,
            password,
            role,
            google
        ];

        const registro = await pool.query('CALL RegistroUsuario(?, ?, ?, ?, ?, ?, ?)', params);
        console.log(registro[0][0]);
        const response = registro[0][0];

        if( !response.status ) {
            return res.json({
                status: response.status,
                message: response.message
            });

        } else {
            return res.json({
                status: response.status,
                data: response
            });
        }

    }
}

export const indexController = new IndexController();

