import { Request, Response, NextFunction } from 'express';
import { webToken } from '../auth/jwtoken/jwtoken';

import path from 'path';
import fs from 'fs';

class VideoController {

    public async index( req: Request, res: Response, next: NextFunction ): Promise<any> {
        
        const file = './src/public/videos/' + req.params.name;

        fs.stat( file, (err, stats) => {
            if(err) {
                if(err.code === 'ENOENT'){
                    return res.sendStatus(404)
                }
                return next(err)
            }
            const range = req.headers.range;
    
            if( !range ) {
                const err = res.json({
                    status: false,
                    message: 'Not authorization'
                })
                return next(err)
            }

            //	4.	Convert the string range in to an array for easy use.
            const positions = range.replace(/bytes=/, '').split('-');
            //	5.	Convert the start value in to an integer
            const start = parseInt(positions[0], 10);
            //	6.	Save the total file size in to a clear variable
            const file_size = stats.size;

            //
            //	7.	IF 		the end parameter is present we convert it in to an
            //				integer, the same way we did the start position
            //
            //		ELSE 	We use the file_size variable as the last part to be
            //				sent.
            //
            const end = positions[1] ? parseInt(positions[1], 10) : file_size - 1;

            //
            //	8.	Calculate the amount of bits will be sent back to the
            //		browser.
            //
            const chunksize = (end - start) + 1;

            //
            //	9.	Create the header for the video tag so it knows what is
            //		receiving.
            //
            const head = {
                'Content-Range': 'bytes ' + start + '-' + end + '/' + file_size,
                'Accept-Ranges': 'bytes',
                'Content-Length': chunksize,
                'Content-Type': 'video/mp4'
            }

            //
            //	10.	Send the custom header
            //
            res.writeHead(206, head);

            //
            //	11.	Create the createReadStream option object so createReadStream
            //		knows how much data it should be read from the file.
            //
            const stream_position = {
                start: start,
                end: end
            }

            //
            //	12.	Create a stream chunk based on what the browser asked us for
            //
            const stream = fs.createReadStream(file, stream_position)

            //
            //	13.	Once the stream is open, we pipe the data through the response
            //		object.
            //
            stream.on('open', () => stream.pipe(res))

            //
            //	->	If there was an error while opening a stream we stop the
            //		request and display it.
            //
            stream.on('error', (err) => next(err) );

        })
    }
}

export const videoController = new VideoController();