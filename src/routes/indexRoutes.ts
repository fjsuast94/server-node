
import { Router } from 'express';

import { indexController } from '../controllers/index.Controller'
import { webToken  } from "../auth/jwtoken/jwtoken";
import { videoController } from '../controllers/video.Controller';

class IndexRoutes {
    public router: Router = Router();
    constructor() {
        this.settings();
    }

    settings(): void {
        this.router.get('/', indexController.index );
        this.router.get('/video/:name', videoController.index );
        this.router.post('/protected', webToken.routeProtected, indexController.index );
    }

}

const indexRoutes = new IndexRoutes();

export default indexRoutes.router;